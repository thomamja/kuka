
package interfaces;

/**
 * Construction of rp->wago packets:
 * B - type of packet,
 *		0 - start
 *		1 - new domino
 *		2 - halt
 *		3 - no more dominos
 *		4 - tip dominos
 * 2B- x coord
 * 2B- y coord
 * 1B- angle
 * 
 * Construction of wago->rp packets:
 *		0 - need new domino
 *              1 - ran out of dominos
 *		255 - error
 */


/**
 *
 * @author Thomas S. Mjåland
 */
public interface Networking extends Runnable {
	
	/**
	 * Initialises a connection to the Wago
	 */
	public boolean start(String ip);
	
	/**
	 * Wether or not the controller is connected
	 * @return true if cotroller is connected, false if not
	 */
	public boolean connected();
	
	/**
	 * Closes the connection to the Wago
	 */
	public void stop();
	
	/**
	 * Sends a start-signal to the controller, prompting it to as for dominos
	 */
	public void send_start();
	
	/**
	 * Sends the point and angle of an domino
	 * @param point_x, the x-value of the domino
	 * @param point_y, the y-value of the domino
	 * @param angle, the angle of the domino
	 */
	public void send_domino(int point_x, int point_y, int angle);
	
	/**
	 * sends stop signal to the robot
	 */
	public void send_abort();
	
	/**
	 * Tells the wago that there isn't any more dominos
	 */
	public void send_done();
	
	/**
	 * Instruct the robot to topple the domino chain
	 */
	public void send_topple();
	
	/**
	 * Assigns a new handler for handling received packets
	 * @param new_handler 
	 */
	public void set_receive_handler(ReceiveHandler newHandler);
	
	public class ReceivedPacket{
		public boolean needsNewDomino = false;
                public boolean outOfDominos = false;
		public boolean remoteError = false;
		public boolean connectionLost = false;
	}
	
	public abstract class ReceiveHandler{
		public abstract void run(ReceivedPacket packet);
	}
}

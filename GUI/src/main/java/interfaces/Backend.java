/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import gui.Coordinate;
import gui.Domino;
import java.util.List;

/**
 *
 * @author Thomas S. Mjåland
 */
public interface Backend {
	
	/**
	 * Returns a list of dominos, ordered by where in the curve the dominos are,
	 * if no points have been given, or they have yet to be processed, 
	 * this returns an empty list, if it has been processed, but with
	 * an insufficient number of points the list might still be empty.
	 * Position of individual dominos are subject to change for each point 
	 * added, regardless of position in the curve.
	 * 
	 * @return A list of all the domino positions calculated during last restructuring
	*/
	public List<Domino> getDominos();
	
	/**
	 * Marks the end of a coherent domino segment, the points added after this call
	 * will not be considered connected to the points before this call, use to
	 * make separate lines possible
	 */
	public void endLine();
	
	/**
	 * Adds a new line-point to the set, also causes an asynchronus 
	 * restructuring of the domino-set, use changed() to detect the completion
	 * of such a restructuring
	 * @param coord, the new point along the line drawn by mouse/touch
	 */
	public void addPoint(Coordinate coord);
	
	/**
	 * Returns true the first time it's called after a restructuring of the
	 * domino-set
	 * @return True if a restructuring has completed since last call to this function
	 */
	public boolean changed();
	
	/**
	 * Sets a handler to handle finished domino-restructurings, can be used for
	 * redrawing of domino display or similar
	 * @param handler, The handler
	 */
	public void setRestructureHandler(RestructureHandler handler);
	
	/**
	 * Removes all the added points stored so far, does not affect any handlers.
	 * getDominos()-calls will return an empty list immediately after calling this
	 * function
	 */
	public void clearPoints();
	
	/**
	 * Checks if there are any dominos that can be drawn or not, should be quicker
	 * than the getDominos().size()-version, as it doesn't have to construct a new
	 * dominolist and copy all existing dominos into it.
	 */
	public boolean isEmpty();
	
	/**
	 * abstract handler class for handling finished domino restructures
	 */
	abstract class RestructureHandler{
		public abstract void run();
	}
}

package gui;

/**
 *
 * @author Thomas S. Mjåland
 */
public class Coordinate {
	Coordinate(int x, int y){
		X=x;
		Y=y;
	}
	/**
	 * The cartesian coordinates the class represents
	 */
	public int X,Y;
}

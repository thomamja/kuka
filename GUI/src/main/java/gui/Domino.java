/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

/**
 *
 * @author Thomas S. Mjåland
 */
public class Domino {
	
	/**
	 * Default constructor
	 */
	public Domino(){}
	
	/**
	 * Constructor
	 * @param nx, The 'new' x
	 * @param ny, The 'new' y
	 * @param nangle, The 'new' angle
	 */
	public Domino(int nx, int ny, int nangle){
		X = nx;
		Y = ny;
		angle = nangle;
	}
	
	/**
	 * Creates a copy of the given domino
	 * @param d, The domino to copy from
	 */
	public Domino(Domino d){
		X = d.X;
		Y = d.Y;
		angle = d.angle;
	}
	
	/**
	 * Cartesian coordinates of the domino
	 */
	public int X,Y;
	
	/**
	 * Angle of domino, between -180 and 180
	 */
	public int angle;
}

package gui;

import java.util.ArrayList;
import java.util.List;

/**
 * Class tasked with handling all point processing and calculating the domino
 * positions and angles.
 *
 * @author Thomas S. Mjåland
 */
public class Backend implements interfaces.Backend {

    private static final Backend instance = new Backend();

    public static Backend getInstance() {
        return instance;
    }

    //list of finalized domino lines
    private final List<Domino> detachedDominoList = new ArrayList<>();

    //list of dominos in current domino line
    private final List<Domino> newDominoList = new ArrayList<>();

    //List of processed points
    private final List<Coordinate> pointList = new ArrayList<>();

    //List of points recently added and not yet processed
    private final List<Coordinate> newPointList = new ArrayList<>();

    //Something's changed, so we should recalculate the domino list
    boolean invalidated = false;

    //We have changed the domino list since last check
    boolean changed = false;

    //Thread tasked with doing the actual heavy lifting
    Thread backgroundWorker = null;

    //Object to call whenever something changes with the dominos
    RestructureHandler restructureHandler = null;

    /**
     * Default constructor, starts the background worker
     */
    Backend() {
        backgroundWorker = new Thread(
                new Runnable() {
            @Override
            public void run() {
                boolean shouldRun;
                while (true) {
                    //If invalidated, then we should run and recalculate the dominos
                    synchronized (Backend.getInstance()) {
                        shouldRun = invalidated;
                    }
                    //If we shouldn't run, then we should pause a bit to offload the poor cpu
                    if (!shouldRun) {
                        try {
                            Thread.sleep(100);
                        } catch (Exception ex) {
                        }
                    } else {
                        //We move the recently added points into the main pointlist to be processed
                        synchronized (newPointList) {
                            synchronized (Backend.getInstance()) {
                                for (Coordinate p : newPointList) {
                                    pointList.add(p);
                                }
                                newPointList.clear();
                            }
                        }
                        //We are no longer invalidated, but this next step takes time, so
                        //this might be set to true again while we're doing this
                        invalidated = false;
                        synchronized (newDominoList) {
                            synchronized (pointList) {
                                synchronized (Backend.getInstance()) {
                                    //We process the pointlist and place the result in the
                                    //newDominoList
                                    processList(pointList, newDominoList, 35);
                                }
                            }
                        }
                        //Something changed, we notify the restructure handler if one is set
                        System.out.println("Running restructure handler");
                        synchronized (Backend.getInstance()) {
                            changed = true;
                            if (restructureHandler != null) {
                                restructureHandler.run();
                            }
                        }
                    }
                }
            }
        }
        );
        //We start the worker, it'll just idle until given a task
        backgroundWorker.start();
        System.out.println("backgroundworker started " + backgroundWorker.isAlive());
    }

    /**
     * Takes a list of points representing a drawn curve, line is processed to
     * fill a list of dominos with position and angle, which can be retrieved by
     * calling getDominos()
     *
     * @param coords, The list of points representing a curve
     * @param _dominoList, The list in which to put the found dominos
     * @param dominodistance, The desired distance between dominoes
     */
    private static void processList(List<Coordinate> coords, List<Domino> _dominoList, int dominoDistance) {
        _dominoList.clear();
		
		if (coords.size()==0)
			return;

        //remove dominos where the spacking is too close
        //Too close - less than 1/10 of dominoDistance
        System.out.println("Spacing too close");
        boolean done = false;
        while (!done) {
            done = true;
            for (int i = Math.max(coords.size() - 15, 0); i < coords.size() - 1; i++) {
                double dist = Math.sqrt(Math.pow(coords.get(i).X - coords.get(i + 1).X, 2) + Math.pow(coords.get(i).Y - coords.get(i + 1).Y, 2));
                if (dist * 10 < dominoDistance) {
                    coords.remove(i + 1);
                    done = false;
                    break;
                }
            }
        }

        //add dominos where the spacing is too long
        //Too long - more that 1/5 of dominoDistance
        System.out.println("Spacking too long");
        done = false;
        while (!done) {
            done = true;
            for (int i = Math.max(coords.size() - 15, 0); i < coords.size() - 1; i++) {
                double dist = Math.sqrt(Math.pow(coords.get(i).X - coords.get(i + 1).X, 2) + Math.pow(coords.get(i).Y - coords.get(i + 1).Y, 2));
                if (dist * 5 > dominoDistance) {
                    coords.add(i + 1, new Coordinate((coords.get(i).X + coords.get(i + 1).X) / 2, (coords.get(i).Y + coords.get(i + 1).Y) / 2));
                    done = false;
                }
            }
        }
        
        //We smooth the points
        for (int j = 0; j < 2; j++) {
            for (int i = Math.max(2, coords.size() - 10); i < coords.size() - 2; i++) {
                Coordinate A, B, C, D;
                A = coords.get(i - 2);
                B = coords.get(i - 1);
                C = coords.get(i + 1);
                D = coords.get(i + 2);

                coords.get(i).X = (A.X + B.X + C.X + D.X) / 4;
                coords.get(i).Y = (A.Y + B.Y + C.Y + D.Y) / 4;
            }
        }
		
		//We find the dominos first
		Coordinate lastDomino = coords.get(0);
		for (int i=1; i<coords.size(); i++){
			double l = Math.sqrt( 
					Math.pow(lastDomino.X-coords.get(i).X,2) +
					Math.pow(lastDomino.Y-coords.get(i).Y,2)
				);
			if (l >= dominoDistance){
				_dominoList.add(new Domino(coords.get(i).X, coords.get(i).Y, 0));
				lastDomino = coords.get(i);
			}
		}
		
		//We then find angles of the dominos based on the position of the dominos in from and behind it
		if (_dominoList.size() >= 2){
			//We handle the first domino, his angle is pointing directly towards the second guy
			_dominoList.get(0).angle = (int) (Math.atan2(_dominoList.get(1).Y - _dominoList.get(0).Y, _dominoList.get(1).X - _dominoList.get(0).X) * 180 / Math.PI)+90;
			for (int i=1; i<_dominoList.size()-1; i++){
					Domino B, C, A;
					B = _dominoList.get(i - 1);
					C = _dominoList.get(i);
					A = _dominoList.get(i + 1);
					//We calculate the angle, the angle is the average of the angle to the next guy and the last guy
					_dominoList.get(i).angle = (int) (((Math.atan2(B.Y - C.Y, B.X - C.X) + Math.atan2(A.Y - C.Y, A.X - C.X)) / 2) * 180 / Math.PI);
			}
			//We handle the last domino, whos angle is pointing directly towards the last guy
			int lastindex = _dominoList.size()-1;
			_dominoList.get(lastindex).angle = (int) (Math.atan2(_dominoList.get(lastindex).Y - _dominoList.get(lastindex-1).Y, _dominoList.get(lastindex).X - _dominoList.get(lastindex-1).X) * 180 / Math.PI)+90;
			
			//We iterate over the angles to ensure that the values are within the accepted bounds, eg. between 0 and 180
			for (int i=0; i<_dominoList.size(); i++){
				Domino d = _dominoList.get(i);
				while (d.angle < 0) {
                    d.angle += 180;
                }

                while (d.angle >= 180) {
                    d.angle -= 180;
                }
			}
		}
		/*
        //We find the domino coordinates and angles
        double dist = 0;
        for (int i = 1; i < coords.size() - 1; i++) {
            dist += Math.sqrt(Math.pow(coords.get(i - 1).X - coords.get(i).X, 2) + Math.pow(coords.get(i - 1).Y - coords.get(i).Y, 2));
            if (dist >= dominoDistance) {
                Coordinate B, C, A;
                B = coords.get(i - 1);
                C = coords.get(i);
                A = coords.get(i + 1);
                //We calculate the angle, right being zero and counter clockwise is positive direction
                int angle = (int) (((Math.atan2(B.Y - C.Y, B.X - C.X) + Math.atan2(A.Y - C.Y, A.X - C.X)) / 2) * 180 / Math.PI);

                //Since a domino at 45 degrees is the same as one at 45+180 degrees, we make the angle go from
                //0 degrees to 180 degrees, thereby making it both easier for the controller, and making the angle
                //fit in a single byte
                while (angle < 0) {
                    angle += 180;
                }

                while (angle >= 180) {
                    angle -= 180;
                }

                _dominoList.add(new Domino(coords.get(i).X, coords.get(i).Y, angle));
                dist -= dominoDistance;
            }
        }
		*/
    }

    /**
     * Constructs a single list of dominos from all the existing domino lines
     * @return A list of all drawn dominos
     */
    @Override
    public List<Domino> getDominos() {
        List<Domino> returnList = new ArrayList<>();
        synchronized (detachedDominoList) {
            synchronized (newDominoList) {
                for (Domino d : detachedDominoList) {
                    returnList.add(new Domino(d));
                }
                for (Domino d : newDominoList) {
                    returnList.add(new Domino(d));
                }
            }
        }
        return returnList;
    }

    /**
     * Cuts off the current domino line, stores it in the list of detached lines
     */
    @Override
    public void endLine() {
        synchronized (detachedDominoList) {
            synchronized (newDominoList) {
                synchronized (newPointList) {
                    synchronized (pointList) {
                        for (Domino d : newDominoList) {
                            detachedDominoList.add(d);
                        }
                        newDominoList.clear();
                        newPointList.clear();
                        pointList.clear();
                    }
                }
            }
        }
    }

    /**
     * Adds a single newly drawn point to the current domino-drawing-line.
     * Also triggers a recalculation of the current domino set.
     * @param coord, The new coordinate to add
     */
    @Override
    public void addPoint(Coordinate coord) {
        synchronized (newPointList) {
            synchronized (Backend.getInstance()) {
                newPointList.add(coord);
                invalidated = true; //To signal that a recalculation must take place
            }
        }
    }

    /**
     * Returns whether the domino set has changed
     * TODO: remove?
     * @return True if the domino set has changed
     */
    @Override
    public boolean changed() {
        return changed;
    }

    /**
     * Sets the handler to be called when something about the domino set changes,
     * to handle redrawings and such
     * @param handler, The instance to handle the restructure event
     */
    @Override
    public void setRestructureHandler(RestructureHandler handler) {
        restructureHandler = handler;
    }

    /**
     * Removes all stored points
     */
    @Override
    public void clearPoints() {
        synchronized (newPointList) {
            synchronized (pointList) {
                synchronized (detachedDominoList) {
                    synchronized (newDominoList) {
                        pointList.clear();
                        newPointList.clear();
                        detachedDominoList.clear();
                        newDominoList.clear();
                        synchronized (Backend.getInstance()) {
                            changed = true;
                            if (restructureHandler != null) {
                                restructureHandler.run();
                            }
                        }
                    }
                }
            }
        }
    }

	/**
	 * Returns whether any dominos have been drawn and therefore whether there
	 * is anything to place out.
	 * @return True if the dominolist is empty, false if not.
	 */
	@Override
	public boolean isEmpty(){
		boolean retval = false;
		synchronized (detachedDominoList) {
			synchronized (newDominoList) {
				retval = detachedDominoList.isEmpty() && newDominoList.isEmpty();
			}
		}
		return retval;
	}
}

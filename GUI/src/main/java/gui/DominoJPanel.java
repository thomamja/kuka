package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author Thomas S. Mjåland
 */
public class DominoJPanel extends JPanel {

    //The size of the real world build-area
    public static int vWidth = 640, vHeight = 250;

    /**
     * Draws the draw-area as well as renders all the current dominos to the screen.
     * @param g, Graphics instance belonging to component, given by the swing framework
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        //paint
        List<Domino> dominos = Backend.getInstance().getDominos();
        JPanel panel = this;
        //We get the actual size of the component
        int width = panel.getWidth();
        int height = panel.getHeight();
        
        //We get the size of the drawing area, as well as displacement in regards to
        //component
        int rx = 0, ry = 0;
        if (width / height > DominoJPanel.vWidth / DominoJPanel.vHeight) {
            rx = (width - (DominoJPanel.vWidth * height) / DominoJPanel.vHeight) / 2;
            width = (DominoJPanel.vWidth * height) / DominoJPanel.vHeight;
        } else {
            ry = (height - (DominoJPanel.vHeight * width) / DominoJPanel.vWidth) / 2;
            height = (DominoJPanel.vHeight * width) / DominoJPanel.vWidth;
        }

        //We draw a gray box showing where the drawing area is
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(rx, ry, width, height);
        g.setColor(Color.black);
        
        //We iterate through all the dominos in the set
        for (int i = 0; i < dominos.size(); i++) {
            Domino domino = dominos.get(i);
            Graphics e = g;

            //We make a four-points polygon (xes for x values, yes for y values),
            //A transformation matrix for the angle (here called the "rotator"),
            //And we set the positions of the polygon points.
            int xes[] = new int[4];
            int yes[] = new int[4];
            double rotator[][] = new double[2][2];
            double ang = ((double) domino.angle) * Math.PI / 180;

            rotator[0][0] = Math.cos(ang);
            rotator[1][0] = -Math.sin(ang);
            rotator[0][1] = Math.sin(ang);
            rotator[1][1] = Math.cos(ang);

            double dsize = 1;//width/vWidth;

			double dominowidth  = 25;
			double dominoheight= 5;
			
            //0 - (-4,1)
            xes[0] = (int) (-dominowidth * 0.5 * dsize * rotator[0][0] + dominoheight * 0.5 * dsize * rotator[1][0]) + domino.X;
            yes[0] = (int) (-dominowidth * 0.5 * dsize * rotator[0][1] + dominoheight * 0.5 * dsize * rotator[1][1]) + domino.Y;

            //1 - (4,1)
            xes[1] = (int) (dominowidth * 0.5 * dsize * rotator[0][0] + dominoheight * 0.5 * dsize * rotator[1][0]) + domino.X;
            yes[1] = (int) (dominowidth * 0.5 * dsize * rotator[0][1] + dominoheight * 0.5 * dsize * rotator[1][1]) + domino.Y;

            //2 - (4,-1)
            xes[2] = (int) (dominowidth * 0.5 * dsize * rotator[0][0] - dominoheight * 0.5 * dsize * rotator[1][0]) + domino.X;
            yes[2] = (int) (dominowidth * 0.5 * dsize * rotator[0][1] - dominoheight * 0.5 * dsize * rotator[1][1]) + domino.Y;

            //3 - (-4,-1)
            xes[3] = (int) (-dominowidth * 0.5 * dsize * rotator[0][0] - dominoheight * 0.5 * dsize * rotator[1][0]) + domino.X;
            yes[3] = (int) (-dominowidth * 0.5 * dsize * rotator[0][1] - dominoheight * 0.5 * dsize * rotator[1][1]) + domino.Y;

            //We map the polygon onto our drawing surface
            for (int k = 0; k < 4; k++) {
                xes[k] = (xes[k] * width) / vWidth + rx;
                yes[k] = (yes[k] * height) / vHeight + ry;
            }
            
            //We render the polygon as solid black shapes
            e.fillPolygon(xes, yes, 4);

        }
    }

}

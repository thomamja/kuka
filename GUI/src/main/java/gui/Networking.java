package gui;

import java.net.InetSocketAddress;
import java.net.Socket;

/**
 *
 * @author Thomas S. Mjåland
 */
public class Networking implements interfaces.Networking {

    public static Networking instance = new Networking();

    private Socket sock = null;
    private ReceiveHandler receiveHandler = null;
    private boolean running = false;
    Thread networkThread = null;

    /**
     * Returns the global Networking instance
     *
     * @return the global Networking instance
     */
    public static Networking getInstance() {
        return instance;
    }

    /**
     * Tries to connect to a given IP with port 2017
     *
     * @param ip, The ip to connect to
     * @return true if successful, false if not
     */
    @Override
    public boolean start(String ip) {
        try {
            try {
                if (sock != null) {
                    sock.close();
                    networkThread.interrupt();
                }
            } catch (Exception ex) {
            }
            InetSocketAddress addr = new InetSocketAddress(ip, 2017);
            sock = new Socket();
            sock.setKeepAlive(true);
            sock.connect(addr, 1000);
            running = true;
            networkThread = new Thread(this);
            networkThread.start();
            return true;
        } catch (Exception ex) {
            System.err.println("unable to open socket: " + ex.getMessage());
            return false;
        }
    }

    /**
     * Whether or not there is currently an active connection. NB: Does NOT
     * actually notice when the other party forcefully disconnects, as there is
     * no pinging implemented as of this moment, will notice a clean close
     *
     * @return true if socket is believed to be open
     */
    @Override
    public boolean connected() {
        if (sock == null) {
            return false;
        }
        return !sock.isClosed();
    }

    /**
     * Sends an abort/cancel signal to the other party, then closes the
     * connection.
     */
    @Override
    public void stop() {
        running = false;
        try {
            if (sock.isConnected()) {
                send_abort();
            }

            sock.close();
            networkThread.interrupt();
        } catch (Exception ex) {
            System.err.println("unable to close socket: " + ex.getMessage());
        }
    }

    /**
     * Sends a start signal to the wago
     */
    @Override
    public void send_start() {
        byte[] startCommand = {(byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
        this.send(startCommand);
    }

    /**
     * Serializes domino-related data and sends it as a domino-packet to the
     * wago
     *
     * @param point_x, The x-coordinate of the domino
     * @param point_y, The y-coordinate of the domino
     * @param angle, The angle of the domino
     */
    @Override
    public void send_domino(int point_x, int point_y, int angle) {
        System.out.println("Sending domino: X: " + point_x + " Y: " + point_y + " Ang: " + angle);
        //NOTE:
        //  it's supposed to be x-y-angle, but since the board is flipped (x and y
        //  swapped) in this program, we here send y-x-angle
        byte[] dominoCommand = {
            (byte) 1,
            (byte) (point_y >> 8),
            (byte) (point_y % 256),
            (byte) (point_x >> 8),
            (byte) (point_x % 256),
            (byte) angle,};
        this.send(dominoCommand);
    }

    /**
     * Sends an abort/cancel signal to the wago
     */
    @Override
    public void send_abort() {
        byte[] haltCommand = {(byte) 2, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
        this.send(haltCommand);
    }

    /**
     * Sends a done-signal to the wago, signifying that there is no more dominos
     * avaliable
     */
    @Override
    public void send_done() {
        byte[] doneCommand = {(byte) 3, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
        this.send(doneCommand);
    }

    /**
     * Sends a topple-singal to the wago NOT YET IMPLEMENTED IN WAGO
     */
    @Override
    public void send_topple() {
        byte[] toppleCommand = {(byte) 4, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
        this.send(toppleCommand);
    }

    /**
     * Sets the handler to run when a packet is received
     *
     * @param newHandler, The object to handle the received packets
     */
    @Override
    public void set_receive_handler(ReceiveHandler newHandler) {
        receiveHandler = newHandler;
    }

    /**
     * The thread-function tasked with reading incoming network data and calling
     * the assigned ReceiveHandler
     */
    @Override
    public void run() {
        int receivedByte;
        while (running) {
            //We read a single byte, that's how large a full wago packet is
            try {
                receivedByte = sock.getInputStream().read();
            } catch (Exception ex) {
                System.err.println("read failed in networking thread");
                //something went wrong, so we forge a packet saying that the connection
                //is lost, and pass it to the packethandler, he'll take care of this.
                ReceivedPacket packet = new ReceivedPacket();
                packet.connectionLost = true;
                receiveHandler.run(packet);
                return;
            }

            ReceivedPacket packet = new ReceivedPacket();

            //populate packet with the received data
            packet.needsNewDomino = (receivedByte == 0);
            packet.outOfDominos = (receivedByte == 1);
            packet.remoteError = (receivedByte == 255);

            //Run ReceiveHandler on the received datapacket
            if (receiveHandler != null) {
                receiveHandler.run(packet);
            }
        }
    }

    /**
     * Simply sends the given array to the socket
     *
     * @param arr, The array of bytes to be sent
     */
    private void send(byte[] arr) {
        try {
            sock.getOutputStream().write(arr);
        } catch (Exception ex) {
            System.err.print("write failed\n    ");
            for (byte b : arr) {
                System.err.print(b + " ");
            }
            System.err.println();
        }
    }

}
